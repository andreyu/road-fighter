# Road Fighter

A remake of the classic MSX game by Brain Games.

## About the Project

In 2003 the people from [Retro Remakes](http://www.remakes.org/) organized a [remake competition](http://www.retroremakes.com/comp2003/results.php) for the first time. The idea was to create a remake from scratch in a short amount of time.

We decided to participate with a remake of the MSX game [Road Fighter](http://www.generation-msx.nl/msxdb/softwareinfo/684): a simple racing game that would be doable before the deadline and still be fun to play (oh the nostalgia!).

In the end we met the deadline (barely!) and sent in our entry. When finally the results came in, we finished 7th out the 83 entries. Not too bad for a 2 month project :)

## Screenshots

![Road Fighter](https://bitbucket.org/andreyu/road-fighter/downloads/road-fighter_0.jpg) ![Road Fighter](https://bitbucket.org/andreyu/road-fighter/downloads/road-fighter_1.jpg)  

## Enhancements & Updates

This version of Road Fighter includes several improvements and fixes:

 - ✅ Fixed crash issues.
 - ✅ Added support for **Linux**, **macOS**, and **Web**.
 - ✅ Optimized surface operations for better performance.
 - ✅ Introduced a simple asset management system.
 - ✅ Refactored and restructured source code and resource hierarchy.
 - ✅ Upgraded to **C++11** standard.
 - ✅ Switched to **CMake** as the build system.

This remake aims to bring the nostalgic racing experience to modern platforms while improving performance and maintainability.

Enjoy the ride! 🚗💨

## Dependencies

 - `CMake` - Cross-platform suite of tools for building, testing, and packaging software.
 - `SDL` v1.2, `SDL_image`, `SDL_mixer`, `SDL_ttf`. 

***

```
Copyright © 2017 Andrey A. Ugolnik. All Rights Reserved.
https://www.ugolnik.info
andrey@ugolnik.info
```